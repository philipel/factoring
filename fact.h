#ifndef _FACT_
#define _FACT_

#include "common.h"

class fact {
public:
	//Create a factorization sieve of size "size"
	fact(int size);

	//Factorize num
	//trialReps,	number of primes used for trial divisions
	//res,			where the resulting factors are stored
	void factorize(mpz_class& num, int trialReps, vector<mpz_class>& res);

	//Returns a reference to the vector storing primes;
	vector<int>& getPrimeList();

private:
	//Largest number that can be quickly factorized
	int maxNum;

	//Vector containing largest prime for number n
	vector<int> factor;

	//Vector containing prime numbers
	vector<int> pList;

	void generate();
};

#endif
