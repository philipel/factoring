from random import randint, choice
from sympy.ntheory import isprime

N = 30
primtal = [None for i in range(N)]
for i in range(2, N):
    if primtal[i] == None:
        primtal[i] = True
        for j in range(2*i, N, i):
            primtal[j] = False
primes = [i for i in range(N) if primtal[i] == True]

#for i in range(10):
    #res = 1
    #for j in range(3):
        #res *= choice(primes)
    #print(res)

large_primes = []
while len(large_primes) < 100:
    n = randint(2**49, 2**50)
    if isprime(n):
        large_primes.append(n)

for i in range(40):
    print(randint(2**99, 2**100))
    print(randint(2**49, 2**50)*randint(2**49, 2**50))

for i in range(10):
    print(choice(large_primes)*choice(large_primes))
