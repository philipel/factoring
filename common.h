#ifndef _COMMON_
#define _COMMON_

#include <iostream>
#include <string>
#include <gmpxx.h>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <assert.h>
#include <bitset>
#include <cmath>
#include <stdint.h>


#ifdef _CONSTANTS0
   #include "constants0.h"

#elif _CONSTANS1
   #include "constants1.h"

#elif _CONSTANS2
   #include "constants2.h"

#elif _CONSTANS3
   #include "constants3.h"

#else
   #include "constants.h"
#endif


#ifdef _DEBUG1
	#define dbg1(m) m
	#define dbg2(m)
#elif _DEBUG2
	#define dbg1(m) m
	#define dbg2(m) m
#else
	#define dbg1(m)
	#define dbg2(m)
#endif

using namespace std;


namespace std
{
	template <typename T> ostream& operator<<(ostream& out, const std::vector<T>& v)
	{
		out << "[" << endl;
		for(int i = 0; i < (int) v.size(); ++i) {
			out << v[i] << endl;
		}
		out << "]" << endl;
		return out;
	}
};

#endif
