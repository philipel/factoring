#include "dynbitset.h"

dynbitset::dynbitset(int size) :
_size(size),
_data(new int[_size])
{
	_capacity = (size >> 5) + ((size & 0x1f) != 0);
	memset(_data, 0, sizeof(_capacity)*_capacity);
}

dynbitset::dynbitset(const dynbitset& other) :
_size(other._size),
_capacity(other._capacity),
_data(new int[_size])
{
	memcpy(_data, other._data, sizeof(_capacity)*_capacity);
}

dynbitset::~dynbitset() {
	delete[] _data;
}

void dynbitset::operator^=(const dynbitset& other) {
	for(int i = 0; i < _capacity; ++i) {
		*(_data + i) ^= *(other._data + i);
	}
}

void dynbitset::flip(const int bitIndex) {
	int mb = 1;
	mb <<= (bitIndex & 0x1f);
	*(_data + (bitIndex >> 5)) ^= mb;
}

void dynbitset::set(const int bitIndex) {
	int mb = 1;
	mb <<= (bitIndex & 0x1f);
	*(_data + (bitIndex >> 5)) |= mb;
}

void dynbitset::unset(const int bitIndex) {
	int mb = 1;
	mb <<= (bitIndex & 0x1f);
	*(_data + (bitIndex >> 5)) &= ~mb;
}

bool dynbitset::test(const int bitIndex) const {
	return (*(_data + (bitIndex >> 5)) >> (bitIndex & 0x1f)) & 1;
}

int dynbitset::size() const {
	return _size;
}

void dynbitset::swap(dynbitset& other) {
	int tsize, tcapacity;
	int* tdata;

	tdata = other._data;
	tsize = other._size;
	tcapacity = other._capacity;

	other._data = _data;
	other._size = _size;
	other._capacity = _capacity;

	_data = tdata;
	_size = tsize;
	_capacity = tcapacity;
}

string dynbitset::str() const {
	string res;
	int b = _size;
	do {
		--b;
		res.push_back((this->test(b) ? '1' : '0'));
	} while(b != 0);
	return res;
}


namespace std
{
	ostream& operator<<(ostream& out, const dynbitset& dbs)
	{
		int b = dbs.size();
		do {
			--b;
			out << dbs.test(b);
		} while(b != 0);
		out << endl;
		return out;
	}
};
