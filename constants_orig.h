#ifndef _CONSTANTS_
#define _CONSTANTS_

//The limit at which a number is considered smooth in the sieve;
const double smoothAcceptanceLimit = 6.3;

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = 2.3;

// largest number (after trial division) to factor with pollard
const mpz_class MAX_POLLARD_RHO = mpz_class("10000000000000000");

// amount of primes to divide by
const int TRIAL_DIVISION = 1500;

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = 30000;

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = 10;

const int MAX_POLLARD_RHO_ITER = 5000;

//Number of precalculated prime factors
const int precalcFactors = 1300000;

#endif
