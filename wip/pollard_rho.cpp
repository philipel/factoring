#include <gmpxx.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include <cassert>
typedef unsigned int ui;
typedef std::vector<ui> vui;
//ui INF = std::numeric_limits<ui>::max();
//#define rep(i, a, b) for(int i=int(a); i<int(b); ++i)

// some random constants
mpz_class MAX_POLLARD_RHO = mpz_class("100000000000000000000000000000");

const int MILLER_RABIN_REPS = 1; // no WA:s with the python code
const ui MAX_TRIAL_DIVISION = 200000;
const ui LSP_SIZE = 15000000;
const ui MAX_POLLARD_RHO_ITER = 120000;
const ui FERMAT_MAX = 100; // no eqn systems with > unknowns
bool DEBUG = false;

/* 
   
   O(size*log(log(size))) tror jag

   result[k] = least prime dividing 'k'
   - for faster factorization.
   uses a sieving method
   TODO: optimize!
   version 1
*/
ui * lsp_array(ui size) {
  ui * res  = new ui[size];
  std::fill(res, res+size, 0);
  for (ui i=2; i<size; i++)
    if (res[i] == 0)
      for (ui pos = i; pos<size; pos+=i) 
        if (res[pos] == 0) res[pos]=i;
  return res;
}

// seems OK
void test_lsp_array_visual() {
  ui * lsp = lsp_array(500);
  for(int i=0; i<500; ++i)
    std::cout << lsp[i] << std::endl;
  delete [] lsp;
}


/*
  version 1, 1000000
  
  real	0m0.027s
  user	0m0.024s
  sys	0m0.000s
-----------------

version 1, 10000000
  
real	0m0.230s
user	0m0.212s
sys	0m0.012s

-----------------
version 1, 100000000


real	0m2.660s
user	0m2.508s
sys	0m0.140s
-----------------
*/
void test_lsp_array_time(ui size) {
  ui * lsp = lsp_array(size);
  delete [] lsp;
}


/*
  O(size*log(log(size))),
  bara för test, lsp ska räknas ut en gång och aldrig
  slängas
 */
vui find_small_primes(ui size) {
  vui res;
  ui * lsp = lsp_array(size);
  for (ui i=2; i<size; ++i)
    if (lsp[i] == i) res.push_back(i);
  delete [] lsp;
  return res;
}



/*
  bara för testande, 
  allt ska genereras i förväg
*/
vui factorize(ui x) {
  vui res;
  ui * lsp = lsp_array(LSP_SIZE);

  if (x >= LSP_SIZE) {
    vui trial = find_small_primes(MAX_TRIAL_DIVISION);
    for (ui i=0; i<trial.size(); ++i) // ui & p: trial)
      while (x%trial[i] == 0) {
        res.push_back(trial[i]);
        x /= trial[i];
      }
  }
  if (x < LSP_SIZE)
    while (x != 1) {
      res.push_back(lsp[x]);
      x /= lsp[x];
    }
  delete [] lsp;
  if (x==1) return res;
  throw new std::range_error("number too large");
}
/*
int main() {
  //test_lsp_array_visual(); // verkar funka...
  //test_lsp_array_time(100000000);
  vui hej = find_small_primes(100);
  for(auto & x: hej)
    std::cout << x << std::endl; // verkar funka...
  
  ui num;
  while (scanf("%u", &num) == 1) 
    for (ui p : factorize(num))
      printf("%u ", p);
  //char a [3][100];
  //scanf("%s %s", a[0], a[1]);
  
  mpz_class A, B, C;
  std::string a, b, c;
  std::cin >> a >> b;
  A= a.c_str(), B = b.c_str();
  C = A+B;
  std::cout << C << std::endl;
  
  mpz_init_set_str(A, a[0], 10);
  mpz_init_set_str(B, a[1], 10);
  mpz_add(C, A, B);
  mpz_get_str(a[2], 10, C);
  printf("%s\n", a[2]);
  //
}
*/


//#include "test.cpp" // should rename this one!
#include <stdlib.h>
//#include <utility>

ui * lsp = NULL; // large array of dividing primes
vui small_primes; // list of small primes


void init_stuff() {
  lsp = lsp_array(LSP_SIZE);
  assert(MAX_TRIAL_DIVISION <= LSP_SIZE);
  for (ui i=2; i<MAX_TRIAL_DIVISION; ++i)
    if (lsp[i]==i) small_primes.push_back(i);
}


//template <typename mpz_class>
void lsp_factor(std::vector<mpz_class> & fct, mpz_class & a) {
  if(DEBUG) assert(a < LSP_SIZE);
  if(DEBUG) assert(lsp != NULL);
  ui aa = (ui)mpz_get_ui(a.get_mpz_t());
  while (aa > 1) {
    fct.push_back((mpz_class)lsp[aa]);
    aa /= lsp[aa];
  }
  a=1;
}

// factors or strips small factors from a
//template <typename mpz_class>
void factor_small(std::vector<mpz_class> & fct, mpz_class & a) {
  if(DEBUG) assert(lsp != NULL);
  if (a<LSP_SIZE) {
    lsp_factor(fct, a);
    return;
  }
  for (ui i=0; i<small_primes.size(); i++) //(ui & p: small_primes)
    while (a%(mpz_class)small_primes[i] == 0 && small_primes[i]<= MAX_TRIAL_DIVISION && a >= LSP_SIZE) { // need not check for p*p<=a since a >= LSP_SIZE takes care of that
      fct.push_back((mpz_class)small_primes[i]);
      a /= (mpz_class)small_primes[i];
    }
  if (a<LSP_SIZE) {
    lsp_factor(fct, a);
    return;
  }
}


/*
  naive implementation of the fermat method
  to look for primes.
  Idea: generate random numbers x,
  check if x**2 mod only has small prime factors,
  combine these x**2 such that you get a square, 
  use conjugate rule.

mpz_class fermat(mpz_class a) {
  mpz_class theROOT;
  int [][] arr = new arr[FERMAT_MAX][FERMAT_MAX]; // what if non-invertible???
  mpz_root(the_ROOT.get_mpz_t(), a.get_mpz_t(), 2);
  for (int a=1; a < 100; a++)
    for (int b=1; b<100; b++) {
      mpz_class val = a*theROOT+b;
      val = (val*val)%a;
    }
  
}
*/

// should eat both uis and mpz_t:s
//template <typename mpz_class>
// returns true if found factor
mpz_class pollard_rho(mpz_class a) {
  if(DEBUG) assert(a<MAX_POLLARD_RHO);
  ui curr = 0;
  ui i = 1, k =2;
  mpz_class d=1;
  mpz_class x = rand()%a;
  if(DEBUG) std::cout << "randval = " << x << std::endl;
  mpz_class y = x;
  while (d==1 && i <= MAX_POLLARD_RHO_ITER) {
    i += 1;
    x = (x*x-1)%a;
    mpz_class z = y-x;
    mpz_gcd(d.get_mpz_t(), z.get_mpz_t(), a.get_mpz_t());
    if (i == k) {
      y=x;
      k *= 2;
    }
  }
  curr += i;
  if (DEBUG) std::cout << "pollard_rho: curr = " << curr << std::endl;
  if (curr > MAX_POLLARD_RHO_ITER) 
    return 0;
  else 
    return d; // much check for 1 or a
  //throw new std::range_error("bad stuff");
}


/*
//
// stolen from the internet for testing... @Author = not me
void f(mpz_t a, mpz_t n){
   mpz_t result;
   mpz_init(result);
   mpz_powm_ui(result, a, 2, n);
   mpz_add_ui(result, result, 123);
   mpz_mod(a, result, n);
}
int pollardrho_brent(mpz_t res, mpz_t n) {
   long long k, r = 1, m = 43;
   mpz_t x, y, ys, q, gcd, tmp;
   mpz_init(gcd);
   mpz_init(tmp);
   mpz_init(ys);
   mpz_init(x);
   mpz_init_set(y,n);
   mpz_init_set_ui(q,1);
   do {
      mpz_set(x, y);
      for (int i=0; i<r; i++)
        f(y, n);
      k = 0;
      do {
         mpz_set(ys, y);
	 for (int i=0; i< std::min(m, r-k); i++){
           f(y, n);
	    mpz_sub(tmp, x, y);
	    mpz_abs(tmp, tmp);
	    mpz_mul(q, q, tmp);
	    mpz_mod(q, q, n);
	 }
	 mpz_gcd(gcd, q, n);
	 k += m;
      }while (k<r && mpz_cmp_ui(gcd,1)<=1);
      r *= 2;
   }while(mpz_cmp_ui(gcd,1)<=0);
   if (mpz_cmp(gcd, n)==0){
      do {
        f(ys, n);
	 mpz_sub(tmp, x, ys);
	 mpz_abs(tmp, tmp);
	 mpz_gcd(gcd, tmp, n);
      }while (mpz_cmp_ui(gcd,1)>0);
   }
   if (mpz_cmp(gcd,n)==0){
      return 0;
   }else{
      mpz_set(res, gcd);
      return 1;
   }
}
*/

void factor(std::vector <mpz_class> & fct, mpz_class & a);

/*
With, my random testcase (no powers)
real	0m30.417s
user	0m30.184s
sys	0m0.092s
-----------------------
Without: same testcase

real	0m30.302s
user	0m30.144s
sys	0m0.052s
=======================
=======================
Without, squares of random 50-bit numbers: (lots of fails)
real	0m20.018s
user	0m19.900s
sys	0m0.024s
-----------------------

With, same testcase: (no fails)

real	0m2.536s
user	0m2.500s
sys	0m0.016s
=======================
With, squares of 2**50-semiprimes: (no fails)
real	0m4.146s
user	0m4.104s
sys	0m0.020s
 */
void factor_perfect_power(std::vector <mpz_class> & fct, mpz_class & a) {
  assert(false);
  if (DEBUG) assert(mpz_perfect_power_p(a.get_mpz_t()) > 0);
  for (ui i=2; ; ++i) {
      mpz_class root;
      mpz_root(root.get_mpz_t(), a.get_mpz_t(), i);
      mpz_class exp;
      mpz_pow_ui(exp.get_mpz_t(), root.get_mpz_t(), i);
      if (exp == a) {
        std::vector<mpz_class> fctroot;
        factor(fctroot, root);
        if (root != 1) assert(false); // should factor anything < 2**50 ?
        for (ui k=0; k<i; ++k) 
          for (ui j=0; j<fctroot.size(); ++j)
            fct.push_back(fctroot[j]);
        a=1;
        return;
      }
      if (exp > a) assert(false);
    }
}



//template <typename mpz_class>
void factor(std::vector <mpz_class> & fct, mpz_class & a) {
  factor_small(fct, a);
  if(DEBUG) std ::cout  << "factor, a = " << a << std::endl;
  if (a==(mpz_class)1) return;
  if (mpz_probab_prime_p(a.get_mpz_t(), MILLER_RABIN_REPS) >= 1) {
    fct.push_back((mpz_class)a);
    a=(mpz_class)1;
    return;
  }
  
  // are none in the input, takes an extra second
  //if (mpz_perfect_power_p(a.get_mpz_t()) > 0) 
  //factor_perfect_power(fct, a);
  
  //return;
  if (a < MAX_POLLARD_RHO)
    while (a != 1) {
      mpz_class d = pollard_rho(a);
      
      if(DEBUG) std::cout << "found " << d << std::endl;
      if (d == 0) return;
      if (d==1 || d == a) continue;
      a /= d;
      //mpz_class b = a/d; // we already stripped small primes in factor_small
      if (d < LSP_SIZE) lsp_factor(fct, d);
      else if (mpz_probab_prime_p(d.get_mpz_t(), MILLER_RABIN_REPS) >= 1) 
        fct.push_back(d);
      else pollard_rho(d);
      if (a < LSP_SIZE) lsp_factor(fct, a);
      else if (mpz_probab_prime_p(a.get_mpz_t(), MILLER_RABIN_REPS) >= 1) {
        fct.push_back(a);
        a=1;
      }
      
    //
    //else pollard_rho(fct, a);
  }
}



int main() {
  //MAX_POLLARD_RHO_GCDS = "1000000"; // max # of gcd evals in single pollard_rho
  init_stuff();
  std::string decimals;
  while (std::cin >> decimals) { // how do you check cin for eof?
    //std::cin >> decimals;
    mpz_class num;
    num = decimals;
    std::vector<mpz_class> fct;
    if(DEBUG) std::cout << "num = " << num << std::endl;
    factor(fct, num);
    if (num == 1)
      for (ui i=0; i<fct.size(); ++i)//(mpz_class p : fct)
        std::cout << fct[i] << std::endl;
    else std::cout << "fail\n";
    std::cout << std::endl;
    
  }
  std::cout << std::flush;
  delete [] lsp;
  return 0;
}
