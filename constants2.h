#ifndef _CONSTANTS_
#define _CONSTANTS_

//The limit at which a number is considered smooth in the sieve;
<<<<<<< Updated upstream
const double smoothAcceptanceLimit = 5.1680353651513515;

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = 3.2366570697069212;

// largest number (after trial division) to factor with pollard
const mpz_class MAX_POLLARD_RHO = mpz_class("79731865492368965632");

// amount of primes to divide by
const int TRIAL_DIVISION = 6740;

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = 64781;

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = 7;

const int MAX_POLLARD_RHO_ITER = 2770;

//Number of precalculated prime factors
const int precalcFactors = 1059125;
=======
const double smoothAcceptanceLimit = 6.1859251392374155;

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = 3.691397342198283;

// largest number (after trial division) to factor with pollard
const mpz_class MAX_POLLARD_RHO = mpz_class("82139087328351092736");

// amount of primes to divide by
const int TRIAL_DIVISION = 3188;

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = 32154;

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = 8;

const int MAX_POLLARD_RHO_ITER = 6142;

//Number of precalculated prime factors
const int precalcFactors = 1441793;
>>>>>>> Stashed changes

#endif
