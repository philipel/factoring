#include "quadsieve.h"

bool isQuadraticResidue(const mpz_class& prime, const mpz_class& num) {
    mpz_class res;
	mpz_class exponent = (prime-1)/2;
	mpz_powm(res.get_mpz_t(), num.get_mpz_t(), exponent.get_mpz_t(), prime.get_mpz_t());
	dbg2(cerr << "QdRes found: ( " << num << " / " << prime << " ) = " << res << endl);
	return res == 1;
}

void factorBase(const mpz_class& num, const int factorLimit, vector<mpz_class>& res) {
	vector<int>& primes = fastFact.getPrimeList();
	mpz_class prime;
	for(int i = 1; i < (int) primes.size() && primes[i] < factorLimit; ++i) {
		prime = primes[i];
		if(mpz_legendre(num.get_mpz_t(), prime.get_mpz_t()) == 1) {
			res.push_back(primes[i]);
		}
	}
}

void primeOffset(const vector<mpz_class>& primeFactors, const mpz_class& num, vector<pair<mpz_class, mpz_class> >& res) {
	mpz_class residue, diff, root, numRoot;
	pair<mpz_class, mpz_class> primeOffsets;
	mpz_root(numRoot.get_mpz_t(), num.get_mpz_t(), 2);
	numRoot++;
	res.resize(primeFactors.size());
	for(int i = 1; i < (int) primeFactors.size(); ++i) {
		residue = num % primeFactors[i];
		root = shanks(primeFactors[i], residue);
		diff = root - numRoot;
		mpz_mod(primeOffsets.first.get_mpz_t(), diff.get_mpz_t(), primeFactors[i].get_mpz_t());
		root = primeFactors[i] - root - numRoot;
		mpz_mod(primeOffsets.second.get_mpz_t(), root.get_mpz_t(), primeFactors[i].get_mpz_t());
		res[i] = primeOffsets;
		dbg2(cerr << "Offset for prime " << primeFactors[i] << " is " << primeOffsets.first << " and " << primeOffsets.second << endl;)
	}
}

void calcTestNumbers(const mpz_class& num, mpz_class& numTest, vector<mpz_class>& res) {
	for(int i = 0; i < (int) res.size(); ++i) {
		res[i] = numTest*numTest - num;
		numTest++;
	}
}

void calcLogTestNumbers(const mpz_class& num, mpz_class& numTest, int testRange, vector<float>& res) {
	mpz_class tmpn;
	double tmpd;
	int i = 0;
	while(i + 10000 < testRange) {
		tmpn = numTest*numTest - num;
		tmpd = log(tmpn.get_d());
		numTest += 10000;
		for(int d = i; d < i+10000; ++d) {
			res[d] = tmpd;
		}
		i += 10000;
	}
	tmpn = numTest*numTest - num;
	tmpd = log(tmpn.get_d());
	numTest += res.size() - i;

	while(i < testRange) {
		res[i] = tmpd;
		i++;
	}
}

mpz_class shanks(const mpz_class prime, const mpz_class& num) {
	dbg1(assert(prime > 2);)
	dbg1(assert(mpz_legendre(num.get_mpz_t(), prime.get_mpz_t()) == 1);)

	mpz_class s, q, z, c, r, t, ttemp, m, b, e, i, two = 2; 
	s = 0;
	q = prime-1;
	while(q % 2 == 0){
		q /= 2;
		++s;	
	} 

	z = 1;
	while(mpz_legendre(z.get_mpz_t(), prime.get_mpz_t()) == 1) {
		z++;
	}

	mpz_powm(c.get_mpz_t(), z.get_mpz_t(), q.get_mpz_t(), prime.get_mpz_t());
	e = (q+1)/2;
	mpz_powm(r.get_mpz_t(), num.get_mpz_t(), e.get_mpz_t(), prime.get_mpz_t());
	mpz_powm(t.get_mpz_t(), num.get_mpz_t(), q.get_mpz_t(), prime.get_mpz_t());
	m = s;

	while(t != 1) {
		i = 1;
		ttemp = (t*t) % prime;
		while(ttemp != 1) {
			i++;
			ttemp = (ttemp*ttemp) % prime;
		}

		e = m-i-1;
		mpz_pow_ui(e.get_mpz_t(), two.get_mpz_t(), e.get_ui());
		mpz_powm(b.get_mpz_t(), c.get_mpz_t(), e.get_mpz_t(), prime.get_mpz_t());
		r = (r*b) % prime;
		t = (t*b*b) % prime;
		c = (b*b) % prime;
		m = i;
	}
	dbg1(assert((r*r) % prime == num));
	dbg2(cerr << "Shank found: " << r << "^2 = " << num << " (mod " << prime << ")" << endl);
	return r;
}

bool createRepresentation(const vector<mpz_class>& factBase, 
	mpz_class possibleSmooth, 
	dynbitset& representation)
{
	for(int f = 0; f < (int) factBase.size() && possibleSmooth != 1 && possibleSmooth >= factBase[f]; ++f) {
		while(possibleSmooth % factBase[f] == 0) {
			possibleSmooth /= factBase[f];
			representation.flip(f);
		}
	}
	return possibleSmooth == 1;
}

void solve(vector<dynbitset>& representation, vector<dynbitset>& res) {
	int rows = representation.size();
	int cols = representation[0].size();
	int row = 0;
	int col = 0;

	vector<dynbitset> operations(rows, dynbitset(rows));
	for(int i = 0; i < rows; ++i) {
		operations[i].set(i);
	}

	while(row < rows && col < cols) {
		int oneAtRow = row;
		while(oneAtRow < rows) {
			if(representation[oneAtRow].test(col)) break;
			oneAtRow++;
		}

		if(oneAtRow == rows) {
			col++;
			continue;
		}

		representation[oneAtRow].swap(representation[row]);
		operations[oneAtRow].swap(operations[row]);
		for(int u = row+1; u < rows; ++u) {
			if(representation[u].test(col)) {
				representation[u] ^= representation[row];
				operations[u] ^= operations[row];
			}
		}
		row++;
		col++;
	}
	res.insert(res.end(), operations.begin()+row, operations.end());
}

mpz_class QSfactor(const mpz_class& num,
	int testRange,
	int extraRows )
{
	vector<mpz_class> factBase(1, 2);
	vector<float> logSieve(testRange);
	vector<float> logPrimes;
	vector<mpz_class> smoothNumbers;
	vector<dynbitset> representations;
	vector<pair<mpz_class, mpz_class> > offset;
	mpz_class numTest, numRoot, wTemp;

	//Calculate the limit for the primes in the factor base
	double logN = log(num.get_d());
	int primeLimit = factBaseMult * exp(0.5 * sqrt(logN*log(logN)));

	//Fill the factor base with primes where (n/p) = 1 and p < primeLimit
	factorBase(num, primeLimit, factBase);
	dbg1(cerr << "Primes in factorbase: " << factBase.size() << endl;)

	//Precalc all the log values of the prime numbers in the factor base
	logPrimes.reserve(factBase.size());
	for(int i = 0; i < (int) factBase.size(); ++i) {
		logPrimes.push_back(log(factBase[i].get_d()));
	}

	//Calculate ceil ( sqrt(n) )
	mpz_root(numRoot.get_mpz_t(), num.get_mpz_t(), 2);
	numRoot++;

	//Precalc the offset for every prime
	primeOffset(factBase, num, offset);

	numTest = numRoot;
	for(int r = 0; (smoothNumbers.size() < factBase.size() + extraRows); r += testRange) {
		calcLogTestNumbers(num, numTest, testRange, logSieve);

		for(int f = 1; f < (int) factBase.size(); ++f) {
			int i;
			for(i = offset[f].first.get_si(); i < testRange; i += factBase[f].get_si()) {
				logSieve[i] -= logPrimes[f];
			}
			offset[f].first = i - testRange;
			for(i = offset[f].second.get_si(); i < testRange; i += factBase[f].get_si()) {
				logSieve[i] -= logPrimes[f];
			}
			offset[f].second = i - testRange;
		}
		for(int i = 0; i < (int) testRange; ++i) {
			if(logSieve[i] < smoothAcceptanceLimit) {
				dynbitset rep(factBase.size());
				wTemp = numRoot;
				wTemp += r;
				wTemp += i;
				wTemp *= wTemp;
				wTemp -= num;
				if(createRepresentation(factBase, wTemp, rep)){
					smoothNumbers.push_back(wTemp);
					representations.push_back(rep);
				}
			}
		}

		dbg1(cerr << "Found a total of " << smoothNumbers.size() << " smooth numbers" << endl;)
	}

	vector<dynbitset> operations;
	solve(representations, operations);

	for(int i = 0; i < (int) operations.size(); ++i) {
		mpz_class lhs = 1, rhs = 1;
		dynbitset& dbs = operations[i];
		for(int b = 0; b < (int) dbs.size(); ++b) {
			if(dbs.test(b)) {
				wTemp = (smoothNumbers[b] + num);
				mpz_root(wTemp.get_mpz_t(), wTemp.get_mpz_t(), 2);
				lhs *= wTemp;
				rhs *= smoothNumbers[b];
			}
		}
		mpz_class rhsRoot;
		mpz_root(rhsRoot.get_mpz_t(), rhs.get_mpz_t(), 2);

		mpz_class factor, gcdPlus = rhsRoot + lhs;
		mpz_gcd(factor.get_mpz_t(), gcdPlus.get_mpz_t(), num.get_mpz_t());
		if(factor != 1 && factor != num) {
			//Success! We have found a factor
			dbg1(cerr << "Found factor:" << factor << endl;)
			return factor;
		}		

		mpz_class gcdMinus = rhsRoot-lhs;
		mpz_gcd(factor.get_mpz_t(), gcdMinus.get_mpz_t(), num.get_mpz_t());
		if(factor != 1 && factor != num) {
			//Success! We have found a factor
			dbg1(cerr << "Found factor:" << factor << endl;)
			return factor;
		}
	}
	return 1;
}
