#ifndef _DYNBITSET_
#define _DYNBITSET_

#include "common.h"

class dynbitset {
public:
	dynbitset(int size);
	dynbitset(const dynbitset& other);

	~dynbitset();

	void operator^=(const dynbitset& other);
	void flip(const int bitIndex);
	void set(const int bitIndex);
	void unset(const int bitIndex);
	bool test(const int index) const;
	int size() const;
	void swap(dynbitset& other);
	string str() const;

private:
	int _size;
	int _capacity;
	int* _data;
};

#endif
