\section{A method invented by Fermat}
The implemented factoring algorithm is a method invented by 
Fermat \cite{cgi-apple}\cite{cgi-contest}.

\subsection*{Main idea}
Suppose we are interested in factoring the number $N$. The idea is to find two numbers $x, y$ such that $x \neq y$ and $x^2\equiv y^2\mod N$. Then it is possible to use $x$ and $y$ to find a nontrivial factor of $N$. $(x-y)(x+y)\equiv 0 \mod N$ some of $gcd(x-y, N), gcd(x+y, N)$ must be a 
nontrivial factor of $N$.

\subsection*{Finding $x, y$}
The difficulty lies in finding these $x, y$.
To generate random numbers $\mod N$ and hope that some of 
them are have equal squares would require an expected value of $O(\sqrt{N})$
random numbers before finding two equal, this can be improved
upon by only considering numbers of the form $\floor{\sqrt{aN}}+b\mod N$,
which are $O(\sqrt{N})$ and the expected number of generated numbers
reduces to $O(\sqrt[4]{N})$.
\cite{cgi-apple} \cite{cgi-contest}\\
\bigRedBox{Appropriate citations, the algorithms book and Håstads notes}
\smallLine
A far better approach uses gaussian elimination over the field $\mathbb{Z}_2$.
We demonstrate this method by example:
Assume we want to factor $711$. We take some random numbers
of the form $\floor{\sqrt{aN}}+b\mod N$
\begin{verbatim}
(floor(a*sqrt(N)+b)^2    = x^2 = y
----------------------------------
(floor(2*sqrt(711))+2)^2 = 55^2 = 181
(floor(-2*sqrt(711))+-3)^2 = 655^2 = 292
(floor(4*sqrt(711))+2)^2 = 108^2 = 288
(floor(-1*sqrt(711))+-3)^2 = 682^2 = 130
(floor(3*sqrt(711))+0)^2 = 79^2 = 553
(floor(4*sqrt(711))+-4)^2 = 102^2 = 450
(floor(-4*sqrt(711))+1)^2 = 606^2 = 360
(floor(3*sqrt(711))+3)^2 = 82^2 = 325
(floor(1*sqrt(711))+2)^2 = 28^2 = 73
(floor(0*sqrt(711))+-3)^2 = 708^2 = 9
\end{verbatim}
Then we factor the squares:
\begin{verbatim}
x^2  = ... = ...*...*...
----------------------
55^2 = 181 = 181
655^2 = 292 = 73*2*2
108^2 = 288 = 3*3*2*2*2*2*2
682^2 = 130 = 13*5*2
79^2 = 553 = 79*7
102^2 = 450 = 5*5*3*3*2
606^2 = 360 = 5*3*3*2*2*2 
82^2 = 325 = 13*5*5
28^2 = 73 = 73
708^2 = 9 = 3*3
\end{verbatim}
Then we try to multiply a subset of the squares $\mod N$ 
in a way such that the resulting number is a square.
We can see that for our case, $130\cdot 360\cdot 325 = 
(13\cdot5\cdot2)\cdot (5\cdot3\cdot3\cdot2\cdot2\cdot2)\cdot (13\cdot5\cdot5)
= 13^2\cdot5^4\cdot 3^2\cdot2^4 = 3900^2$ works.
We then calculate the gcd:
\begin{verbatim}
>>> M = 130*360*325
>>> sq = int(sqrt(M))
>>> N = 711
>>> K = 682*606*82
>>> gcd(sq-K, N)
9
\end{verbatim}
which is a nontrivial factor!
\smallLine
The step where we chose which numbers to multiply
is actually equivalent to solving a linear system
of equations over $\mathbb{Z}_2$:
Assume that the square number $y$ we are looking for
has the form
$2^{x_2}3^{x_3}5^{x_5}\ldots181^{x_{181}}$.
Let $x_1,\ldots, x_k$ be the small random numbers we generate,
in our example, $x_1, \ldots, x_{10} = 55,\ldots, 708$.
We are looking for a subset of $S\subseteq\{1,\ldots, 10\}$ 
such that 
\[y^2 = \prod_{i\in S}x_i\] is a perfect square.
Then we can set 
\[x = \prod_{i\in S}(x_i^2\mod N)\]
and hope that
$\gcd(x\pm y, N)$ is a nontrivial factor of $N$.
\smallLine
In our case, $S = \{4,7,8\}, x = 688\cdot 606\cdot 82$ and
$y = \sqrt{130\cdot 360\cdot 325} = 3900$
\smallLine
$M$ be the $10$-rows matrix with
Let $x_i = 2^{M_{i,2}}3^{M_{i, 3}}5^{M_{i, 5}}\ldots181^{M_{i, 181}}$
Finding the set $S$ is equivalent to finding a subset of the rows
of the matrix $M$ with every coefficient in the vector-sum even.
This is the same as solving $M\cdot X = \mathbf{0}$, which 
can be done by gaussian elimination.
\section{Implementation details}
This algorithm was implemented in \texttt{C++} with \texttt{gmp} for
large numbers.
We used 
\begin{itemize}
\item A lookup array \verb+A+ for factoring numbers $\leq 15\cdot 10^6$
  with \verb+A[i]+ containing the largest prime factor of \verb+A[i]+
\item We kept generating and factoring $x_i$:s and kept the ones
  that were products of the first $90$ primes.
  \verb+std::vector<std::bitset<90> >+ was used for the matrix
\item
  The matrix had dimensions $(90+30)\times 90$.
\item
  Finding the kernel was done using the following algorithm:
  Let 
  \[M = \left(\begin{array}{ccc}
      1 & 1 & 0\\
      0 & 0 & 1\\
      1 & 0 & 1\\
      1 & 1 & 1\end{array}\right) \]
  We perform row reduction on $M$ by applying row operations \texttt{swap} and
  \texttt{add} until we get zero rows. Another matrix keeps track of 
  what rows were added/swapped to archive a zero row:

  \begin{eqnarray*}
    \left(\begin{array}{ccc}
        1 & 1 & 0\\
        0 & 0 & 1\\
        1 & 0 & 1\\
        1 & 1 & 1\end{array}\right|\left. 
      \begin{array}{cccc}
        1 & 0 & 0 & 0\\
        0 & 1 & 0 & 0\\
        0 & 0 & 1 & 0\\
        0 & 0 & 0 & 1\end{array}\right) \sim
    \left(\begin{array}{ccc}
        1 & 1 & 0\\
        0 & 0 & 1\\
        0 & 1 & 1\\
        0 & 0 & 1\end{array}\right|\left.
      \begin{array}{cccc}
        1 & 0 & 0 & 0\\
        0 & 1 & 0 & 0\\
        1 & 0 & 1 & 0\\
        1 & 0 & 0 & 1\end{array}\right)\\
    \sim
    \left(\begin{array}{ccc}
        1 & 1 & 0\\
        0 & 1 & 1\\
        0 & 0 & 1\\
        0 & 0 & 1\end{array}\right|\left.
      \begin{array}{cccc}
        1 & 0 & 0 & 0\\
        1 & 0 & 1 & 0\\
        0 & 1 & 0 & 0\\
        1 & 0 & 0 & 1\end{array}\right)\sim
    \left(\begin{array}{ccc}
        1 & 1 & 0\\
        0 & 1 & 1\\
        0 & 0 & 1\\
        0 & 0 & 0\end{array}\right|\left.
      \begin{array}{cccc}
        1 & 0 & 0 & 0\\
        1 & 0 & 1 & 0\\
        0 & 1 & 0 & 0\\
        1 & 1 & 0 & 1\end{array}\right)
  \end{eqnarray*}
  The second matrix keeps track of what rows to add to get zero combinations,
  in this case, it is $(1,1,0,1)$, meaning that we add the first, second
  and fourth row to get $0$.
\item
  Trial division by the first couple of thousand primes.
\end{itemize}

%\includeCode[python3] {factor.py}
