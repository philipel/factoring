%% \section{A method invented by Fermat}
%% The implemented factoring algorithm is a method invented by 
%% Fermat.

%% \subsection*{Main idea}
%% Suppose we are interested in factoring the number $N$. The idea is to find two numbers $x, y$ such that $x-y\not\equiv 0 \mod N, x+y\not\equiv 0\mod N$ and $x^2\equiv y^2\mod N$. Then it is possible to use $x$ and $y$ to find a nontrivial factor of $N$. $(x-y)(x+y)\equiv 0 \mod N$, and it follows that some of $gcd(x-y, N), gcd(x+y, N)$ must be a nontrivial factor of $N$.

%% \subsection*{Finding $x, y$}
%% \label{sseq:dixon}
%% The difficulty lies in finding these $x, y$ values.
%% To generate random numbers $\mod N$ and hope that some of 
%% them are have congruent squares would require testing 
%% an expected value of $O(\sqrt{N})$
%% random numbers before finding two that are equal. This can be improved
%% upon by only considering numbers of the form $\ceil{\sqrt{aN}}+b\mod N$,
%% which are $O(\sqrt{N})$ for small $a, b$ and the expected number of generated
%% numbers reduces to $O(\sqrt[4]{N})$.\\
%% \bigRedBox{Appropriate citations, the algorithms book and Håstads notes}
%% \smallLine
%% A far better approach uses gaussian elimination over $\mathbb{Z}_2$.
%% We demonstrate this method by example:
%% Assume we want to factor $711$. We take some random numbers
%% of the form $\ceil{\sqrt{aN}}+b\mod N$
%% \begin{verbatim}
%% (ceil(a*sqrt(N)+b)^2    = x^2 = y
%% ----------------------------------
%% (ceil(2*sqrt(711))+2)^2 = 55^2 = 181
%% (ceil(-2*sqrt(711))+-3)^2 = 655^2 = 292
%% (ceil(4*sqrt(711))+2)^2 = 108^2 = 288
%% (ceil(-1*sqrt(711))+-3)^2 = 682^2 = 130
%% (ceil(3*sqrt(711))+0)^2 = 79^2 = 553
%% (ceil(4*sqrt(711))+-4)^2 = 102^2 = 450
%% (ceil(-4*sqrt(711))+1)^2 = 606^2 = 360
%% (ceil(3*sqrt(711))+3)^2 = 82^2 = 325
%% (ceil(1*sqrt(711))+2)^2 = 28^2 = 73
%% (ceil(0*sqrt(711))+-3)^2 = 708^2 = 9
%% \end{verbatim}
%% Then we factor the squares:
%% \begin{verbatim}
%% x^2  = ... = ...*...*...
%% ----------------------
%% 55^2 = 181 = 181
%% 655^2 = 292 = 73*2*2
%% 108^2 = 288 = 3*3*2*2*2*2*2
%% 682^2 = 130 = 13*5*2
%% 79^2 = 553 = 79*7
%% 102^2 = 450 = 5*5*3*3*2
%% 606^2 = 360 = 5*3*3*2*2*2 
%% 82^2 = 325 = 13*5*5
%% 28^2 = 73 = 73
%% 708^2 = 9 = 3*3
%% \end{verbatim}
%% Then we try to multiply a subset of the squares $\mod N$ 
%% in a way such that the resulting number is a square.
%% We can see that for our case, $130\cdot 360\cdot 325 = 
%% (13\cdot5\cdot2)\cdot (5\cdot3\cdot3\cdot2\cdot2\cdot2)\cdot (13\cdot5\cdot5)
%% = 13^2\cdot5^4\cdot 3^2\cdot2^4 = 3900^2$ works.
%% We then calculate the gcd:
%% \begin{verbatim}
%% >>> M = 130*360*325
%% >>> sq = int(sqrt(M))
%% >>> N = 711
%% >>> K = 682*606*82
%% >>> gcd(sq-K, N)
%% 9
%% \end{verbatim}
%% which is a nontrivial factor!
%% \smallLine
%% The step where we chose which numbers to multiply
%% is actually equivalent to solving a linear system
%% of equations over $\mathbb{Z}_2$:
%% Assume that the square number $y$ we are looking for
%% has the form
%% $2^{x_2}3^{x_3}5^{x_5}\ldots181^{x_{181}}$.
%% Let $x_1,\ldots, x_k$ be the small random numbers we generate,
%% in our example, $x_1, \ldots, x_{10} = 55,\ldots, 708$.
%% We are looking for a subset of $S\subseteq\{1,\ldots, 10\}$ 
%% such that 
%% \[y^2 = \prod_{i\in S}x_i\] is a perfect square.
%% Then we can set 
%% \[x = \prod_{i\in S}(x_i^2\mod N)\]
%% and hope that
%% $\gcd(x\pm y, N)$ is a nontrivial factor of $N$.
%% \smallLine
%% In our case, $S = \{4,7,8\}, x = 688\cdot 606\cdot 82$ and
%% $y = \sqrt{130\cdot 360\cdot 325} = 3900$
%% \smallLine
%% Let $M$ be the $10$-rows matrix with
%% $x_i = 2^{M_{i,2}}3^{M_{i, 3}}5^{M_{i, 5}}\ldots181^{M_{i, 181}}$
%% Finding the set $S$ is equivalent to finding a subset of the rows
%% of the matrix $M$ with every coefficient in the vector-sum even.
%% This is the same as solving $M\cdot X = \mathbf{0}$, which 
%% can be done by gaussian elimination.
%% \smallLine
The factorization algorithm in the form described above 
is called \textit{Dixon's algorithm}. 
In \textbf{section \ref{sseq:sieve}}
we discuss ways to generate numbers $x$ with only small factors
to limit the size of the matrix and the time spent on finding
candidates $x$.
\\\\
%\includeCode[python3] {factor.py}
\subsection{Analysis of Fermat's method}
Following \cite{haastad} \textbf{section 4.4} we define a number $x$ to be $B$-smooth if it has no prime factors larger than $B$.
The algorithm depends on finding enough $B$-smooth numbers, which 
is the most expensive operation, so the key step to 
prove some bounds on the running time is a good
estimate of the time needed to find a $B$-smooth number.
Let $\Psi(x, B)$ be the number of integers $\leq x$
that are  $B$-smooth.
The factoring algorithm finds numbers 
bounded by  $C \ceil{ \sqrt{N}}$ for some (small) constant $C$
and filters out the $B$-smooth ones.
We assume that the numbers are drawn randomly (that the numbers
$\left(\ceil{\sqrt{aN}}+b\right)^2\mod N$ are sufficiently randomly 
distributed so that 
the probability that a number of
the form $x = \left(\left(\ceil{\sqrt{aN}}+b\right)^2\mod N\right)$
is $B$-smooth is 
$\frac{\Psi\left(x, B\right)} {x}$).
\\\\
The algorithm finds  $\pi(B)+k$ numbers, where $\pi(B)$ is 
the number of primes less than or equal to $ B$ and $k$ a small constant.
To check that a number is $B$-smooth takes $\pi(B)$ divisions,
and to find one we can expect to to try
$O\left(\frac{x}{\Psi\left(x, B\right)}\right)$ non-smooth ones.
We still have to factor both the non-smooth and the smooth numbers,
so the total time to find $\pi(B)$ $B$-smooth numbers is 
\begin{equation}
O\left(\frac{x\pi(B)^2}{\Psi\left(x, B\right)}\right)\label{eq:smooth-time}
\end{equation}
\cite {wolfram-smooth} claims that 
\cite{canfield-erdos} proves\footnote{The article doesn't contain any of the words 
\textit{minimum, minima, minimal, minimize\ldots}, 
nor does it contain any formula that looks anything like
(\ref{eq:best-factor-base}) or (\ref{eq:smooth-time}).
It approximates $\Psi(x, y)$ for different special values 
of $x$ and $y$, and since $\pi(B)\sim \frac{B}{\log B}$ 
it should now be possible to use equations (3.1) and (3.2) or something similar
of \cite{canfield-erdos} to prove that our value of $B$ is
the correct one. \cite{tale-two-sieves} (page 1477) refers to 
\cite{canfield-erdos} and claims (without proof) that (\ref{eq:best-factor-base})
and (\ref{eq:min-of-smooth-time}) holds.
}
that the value of $B$ that minimizes
(\ref{eq:smooth-time}) is 
\begin{equation}
B \in O\left(e^{\frac{1}{2}\sqrt{\ln x \ln(\ln(x))}}\right)\label{eq:best-factor-base}
\end{equation}
and that the minimum is then
\begin{equation}
O\left(e^{2\sqrt{\ln x \ln(\ln(x))}}\right)\label{eq:min-of-smooth-time}
\end{equation}
In our case, $x \in O(\sqrt{N})$, we plug it in and get
\begin{equation}
O\left(e^{2\sqrt{\frac{1}{2}\ln N \ln(\frac{1}{2}\ln(N))}}\right)
= 
O\left(e^{\sqrt{2\ln N \ln(\ln(N))}}\right)
\label{eq:min-time-to-factor}
\end{equation}

\section{Sieving}
\subsection{Not all primes need to be considered}
We are factoring numbers $x^2\mod N$ of the form
$x =\left(\ceil{\sqrt{bN}}+a\right)^2\mod N$.
For the algorithm below, we take $b=1$,
(it will still generate enough relatively small numbers, 
out of which sufficiently many will be $B$-smooth),
which for small $a$ (and we only have small values of $a$, 
otherwise the algorithm takes too long and the analysis (\ref{eq:best-factor-base})
is wrong) is
$$x=\left(\ceil{\sqrt{N}}+a\right)^2-N$$
A prime $p$ is a factor of $x$ if and only if
$$\left(\ceil{\sqrt{N}}+a\right)^2\equiv N\mod p$$
so $N$ is the square of some residue $\mod p$, so we only 
need to consider the primes $p$ for which $N\equiv r^2\mod p$ 
has a solution in $r$.
\\\\
$g(r) = r^2\mod p$ has $g(r)=g(-r)$ and $g(a) = g(b) \Llra a=\pm b$,
which leads to $\left|g\left(\mathbb{Z}_p\right)\right|=\frac{p+1}{2}$
for odd primes $p$, so every second number $N$ has a solution 
in $r$ for a given odd prime $p$. We therefore expect roughly every second
prime $p$ to work.
\\\\
Less primes to be considered leads to an improvement 
by at least a factor of $2$:
we no longer need $\pi(B)$ many $B$-smooth numbers but only
about $\frac{\pi(B)}{2}$.
Or, we could increase $B$ from the $\pi(B)$:th prime to 
be the $2\pi(B)$:th (which is at least $2B$) and get a larger
proportion $B$-smooth numbers.
\subsection{The sieve}\label{sseq:sieve}
Instead of iterating though values of $a$ and factoring every
$$x=\left(\ceil{\sqrt{N}}+a\right)^2-N$$
by trial division (which takes about $\pi(B)$ steps for each value of $x$)
and tests order of 
\[\frac{e^{\sqrt{2\ln N \ln(\ln(N))}}} {\pi(B)^2}\]
many values of $x$ we can use a sieving method
that avoids trial division and is both faster 
in practice and asymptotically.
\\\\
We first choose a bound $B\sim e^{\frac{1}{2}\sqrt{\ln x \ln(\ln(x))}} $ according to (\ref{eq:best-factor-base}),
then filter out the primes that are not quadratic residues mod $N$
and call the remaining primes \textit{the factor base}.
The goal is to find numbers with high probability of being $B$-smooth
without having to factor them by trial division.
Let $f(x) = \left(\ceil{\sqrt{N}}+x\right)^2-N$.
The algorithm starts by filling a large array $A$ with zeros.
After the algorithm has been run, $A[i]$ will contain the value
$\log(f(i))$ for most $B$-smooth numbers and a value less than 
$\log(f(i))$ for all non-$B$-smooth numbers.
For each prime $p$ in the factor base, we 
solve the equation $f(x) \equiv 0\mod p$ (see \textbf{section \ref{ssseq:quad-torinelli-shanks} } for how to do it).
The solutions are periodic with period $p$ because $f$ is 
a polynomial with integer coefficients.
We then add $\log p$ to every $A[x]$ for which $p|f(x)$.
After this, every number $A[x]$ contains the value
$\log p_1+\log p_2+\ldots$ for each $p_i$ where 
\[p_i\in\text{factor base and } p_i|f(x)\]
Then for every $B$-\textit{smooth} number $f(x)$ without multiple factors 
$A[x]$ has the value $\log f(x)$ and every non-$B$-smooth number
contains a value less than $\log f(x)$.
\subsubsection{Analysis}
The array has to contain about
$L =\frac{e^{\sqrt{2\ln N \ln(\ln(N))}}} {\pi(B)^2}$
numbers (or a little more because we cannot detect $B$-smooth numbers
with many multiples of the same primes).
For each prime $p$ of the $\pi(B)$ primes, we have to 
solve the quadratic equation, which according to 
\textbf{section \ref{ssseq:quad-torinelli-shanks} } can be 
done in $O(\log(p)^2)$. Then we perform $L/p$ additions.
The total time spent adding and solving equations is
$O\left(\sum_{p\in\text{factor base}} \frac{L}{p} + \log(p)^2\right)$.
Initializing, comparing and solving matrix equation is 
additional
$O\left(L + \pi(B)^3\right)$.
A known fact in analytic number theory is that
\[\sum_{p \text{ prime}}^B = O(\log(\log(B)))\]
so the dominating term is 
$O\left(L\log(\log(B))\right)$.
This is a heuristic argument that the expected 
running time of the algorithm is 
\begin{eqnarray*}
O\left(L\log(\log(B))\right) = 
O\left(\frac{e^{\sqrt{2\ln N \ln(\ln(N))}}} {\pi(B)^2}\log(\log(B))\right) = \\
O\left(B^{-2}e^{\sqrt{2\ln N \ln(\ln(N))}}\log^2 B\log\log B\right) = \\
\label{eq:tot-run-time}
\end{eqnarray*}
with $B = O(e^{\frac{1}{2}\sqrt{\ln x \ln(\ln(x))}})$ and $x=\sqrt{N}$

\subsubsection{Solving $f(x)=(R+x)^2\equiv 0\mod p$}
\label{ssseq:quad-torinelli-shanks}
The \textbf{Tonelli-Shanks algorithm} finds the two roots
in $O(\log(p)^2)$ average time.
\\\\
Brief explanation: write $p-1 = Q\cdot 2^S$ with $Q$-odd.
\begin{itemize}
\item We want to find $R$ with $R^2\equiv n$.
\item There is a simple method to get a congruence class 
  with order $2^t$ for any $t<S$:
  take any of the $\frac{p-1}{2}$ quadratic non-congruences
  (e.g. by drawing random numbers or trying numbers 
  $2,3,5,6,7,8,10\ldots$ 
  (there is a theorem that says that you don't have to try too many)
  and raise it to the power $Q\mod p$.
  Then the resulting number will have order $2^S$).
\item The algorithm starts with 
  $r^2 = nt$ for some $r, t$ with the property that
  $ord(t) = 2^k$ for some $k\leq S$.
\item
  It maintains $r^2=nt$ changing $r$ and $t$.
\item We change $r$ to $r\cdot b$ where $b$ has order $2^{(k+1)}$ 
  and $t$ so that $r^2=nt$ holds again
  (we multiply the old $t$ by $b^{-2}$.
\item
  Then one can prove that
  $t$ has order $2^{k'}$ with $k'<k$, so we continue until $ord(t) = 1$
  and $r^2=n$.
\end{itemize}


