#include "common.h"
#include "fact.h"
#include "quadsieve.h"
#include "dynbitset.h"
#include "pollard_rho.h"

/*
  fastFact(n) is a sieve that factors numbers m <= n
  in worst-case log(m), average case log(log(m)),
  O(n) memory
 */
fact fastFact(precalcFactors);

/*
  Factors num, adds factors to res.
  Handles special cases  prime and perfect power.
  calls factor(x, y, z) from that factors using QS
*/
bool factorNum(mpz_class num, vector<mpz_class>& res, bool doPollard=true) {
	if(mpz_probab_prime_p(num.get_mpz_t(), 1)) {
		dbg1(cerr << "Prime: " << num << endl;)
		res.push_back(num);
		return true;
	}
        
	int perfecPower = mpz_perfect_power_p(num.get_mpz_t());
	if(perfecPower) {
		dbg1(cerr << "peftect power: " << num << endl;)
		mpz_class wTemp;
		int i = 1;
		while(!mpz_root(wTemp.get_mpz_t(), num.get_mpz_t(), ++i));
		bool ok = true;
		while(i--) {
			ok &= factorNum(wTemp, res);
		}
		return ok;
	}
        
        mpz_class fact;
        if (doPollard) {
          fact = PollardRhoFactorizer().findFactor(num);
          if (fact != 0) return factorNum(num/fact, res) && factorNum(fact, res);
        }
        
        fact = QSfactor(num, (TEST_RANGE/1000)*1000, EXTRA_ROWS);
	if(fact == 1) return false;
	return factorNum(num/fact, res,false) && factorNum(fact, res,false);
}

int main() {
	string sNum;
	while(cin >> sNum) {
		vector<mpz_class> res;
		mpz_class num(sNum);

		if(num == 1) {
			cout << "1" << endl << endl;
			continue;
		}

		fastFact.factorize(num, TRIAL_DIVISION, res);
		bool ok = true;
		if(num != 1) {
			ok = factorNum(num, res);
		}
		if(ok) {
			for(int i = 0; i < (int) res.size(); ++i) {
				cout << res[i] << endl;
			}
		} else {
			cout << "fail" << endl;
		}

		cout << endl;
		
	}
	return 0;
}
