#ifndef _CONSTANTS_H
#define _CONSTANTS_H
//The limit at which a number is considered smooth in the sieve;
const double smoothAcceptanceLimit = 6.660024554858706;

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = 2.5669661837129567;

//Number of precalculated prime factors
const int precalcFactors = 1022924;

//Number of times the prime test is repeted
const int primetestReps = 9;

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = 29451;

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = 5;

// amount of primes to divide by
const int TRIAL_DIVISION = 1539;
#endif
