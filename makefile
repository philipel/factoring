FLAGS = -Wall -lgmpxx -lgmp

default: debug1

release:
	g++ *.cpp -O2 -ggdb $(FLAGS) -o fct

fct%: constants0.h constants1.h constants2.h constants3.h
	g++ *.cpp -O2 -D_CONSTANTS$* $(FLAGS) -o fct$*

debug1:
	g++ *.cpp -ggdb -D_GLIBCXX_DEBUG -D_DEBUG1 $(FLAGS) -o fct

odebug1:
	g++ *.cpp -O2 -g -ggdb -D_GLIBCXX_DEBUG -D_DEBUG1 $(FLAGS) -o fct

debug2:
	g++ *.cpp -ggdb -D_GLIBCXX_DEBUG -D_DEBUG2 $(FLAGS) -o fct

profile:
	g++ *.cpp -O2 -pg -ggdb $(FLAGS) -o fct

# It claims 'report' up to date, the dependencies don't help, have to make with '-B'
report: report/report.tex report/template.tex
	$(MAKE) -C report	

submit:
	./submit.py -f -p oldkattis:factoring fact.cpp main.cpp pollard_rho.cpp quadsieve.cpp dynbitset.cpp common.h fact.h pollard_rho.h quadsieve.h dynbitset.h constants.h
	echo "SUBMIT SUBMIT SUBMIT!!!"