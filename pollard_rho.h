#ifndef _POLLARD_RHO_
#define _POLLARD_RHO_
#include <gmpxx.h>

class PollardRhoFactorizer {
public:
	/* Returns 0 or nontrivial divisor of T */
	mpz_class findFactor(const mpz_class & num);

private:
	static const int MAX_POLLARD_RHO_ITER=2000;

};

#endif
