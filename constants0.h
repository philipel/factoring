#ifndef _CONSTANTS_H
#define _CONSTANTS_H
//The limit at which a number is considered smooth in the sieve;
const double smoothAcceptanceLimit = 6.453660243309206;

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = 2.223780314455539;

//Number of precalculated prime factors
const int precalcFactors = 1076445;

//Number of times the prime test is repeted
const int primetestReps = 10;

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = 29396;

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = 5;

// amount of primes to divide by
const int TRIAL_DIVISION = 1568;
#endif
