#ifndef _QUADSIEVE_
#define _QUADSIEVE_

#include "common.h"
#include "fact.h"
#include "dynbitset.h"

extern fact fastFact;


bool isQuadraticResidue(const mpz_class& prime, const mpz_class& num);

void factorBase(const mpz_class& num, const int factorLimit, vector<int>& res);

mpz_class shanks(const mpz_class prime, const mpz_class& num);

mpz_class QSfactor(const mpz_class& num,
	int testRange,
	int extraRows);


#endif

