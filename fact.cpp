#include "fact.h"

fact::fact(int size) :
maxNum(size)
{
	factor.resize(size);
	generate();
}

void fact::generate() {
	int primesFound = 0;
	for(size_t i = 2; i < factor.size(); ++i) {
		if(factor[i] == 0) {
			pList.push_back(i);
			primesFound++;
			for(size_t n = i; n < factor.size(); n += i) {
				factor[n] = i;
			}
		}
	}
}

void fact::factorize(mpz_class& num, int trialReps, vector<mpz_class>& res) {
	dbg1(assert(num != 0));
	for(int p = 0; p < (int) pList.size() && trialReps--; ++p) {
		while(num % pList[p] == 0) {
			res.push_back(pList[p]);
			num /= pList[p];
		}
	}
}

vector<int>& fact::getPrimeList() {
	return pList;
}
