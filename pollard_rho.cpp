#include "pollard_rho.h"
#include <stdlib.h>

mpz_class PollardRhoFactorizer::findFactor(const mpz_class& num) {
  int curr = 0;
  int i = 1, k =2;
  mpz_class d=1;
  mpz_class x = rand()%num;
  mpz_class y = x;
  while (d==1 && i <= MAX_POLLARD_RHO_ITER) {
    i += 1;
    x = (x*x-1)%num;
    mpz_class z = y-x;
    mpz_gcd(d.get_mpz_t(), z.get_mpz_t(), num.get_mpz_t());
    if (i == k) {
      y=x;
      k *= 2;
    }
  }
  curr += i;
  if (curr > MAX_POLLARD_RHO_ITER || d==num) 
    return 0;
  else 
    return d; // much check for 1 or a
}
