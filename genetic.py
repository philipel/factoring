from subprocess import call, check_output, STDOUT
from random import gauss
import threading
from time import sleep
from itertools import count

small_step, large_step = 0.05, 0.2

format_str="""#ifndef _CONSTANTS_H
#define _CONSTANTS_H
//The limit at which a number is considered smooth in the sieve;
const double smoothAcceptanceLimit = {};

//The multiplier used when calculating the limit of the primes in the factor base
const float factBaseMult = {};

//Number of precalculated prime factors
const int precalcFactors = {};

//Number of times the prime test is repeted
const int primetestReps = {};

// numbers in each sieve-block for finding smooth
const int TEST_RANGE = {};

// the matrix is n*(n+EXTRA_ROWS) large
const int EXTRA_ROWS = {};

// amount of primes to divide by
const int TRIAL_DIVISION = {};
#endif
"""

START_PARAMS = [6.3, 2.4, 1000000, 10, 30000, 5, 1500]

VERBOSE = True

def write(params, th):
    f = open("constants{}.h".format(th), "w")
    params = params[:2] + [int(x) for x in params[2:]]
    f.write(format_str.format(*params))
    f.close()


def compile(params, th):
    write(params, th)
    call(["make", "-s", "fct{}".format(th)])

def run(params, th, where):
    call("time -p -o res{} ./fct{} < testdata/test_genetic >> /dev/null".format(th, th), stderr = STDOUT, shell=True, universal_newlines=True)
    f = open('res{}'.format(th), 'r')
    try:
        time = float(f.readlines()[0].split() [1])
    except ValueError: 
        return
    f.close()
    if VERBOSE:
        print(time, end=" ")
    #where.append((time, params))

def mutate(params, width):
    return [x*gauss(1, width) for x in params]

BEST_TIME, BEST_PARAMS = 10**100, None

N_THREADS = 2

def next_generation(last_generation, kids_per_point, it, N):
    global BEST_TIME
    global BEST_PARAMS
    if VERBOSE:
        print("\n\n------------------\nGeneration", it)
        print("Best time is {} with params {}\n--------------------".format(BEST_TIME, BEST_PARAMS))
    res = []
    for params, idx in zip(last_generation, count()):
        if VERBOSE: print("\n-----------------\nProcessing element", idx)
        
        for i in range(kids_per_point//N_THREADS):
            threads, compile_threads = [], []
            for j in range(N_THREADS):
                kid = mutate(params, width=(large_step if i%4==1 else small_step))
                
                cthread = threading.Thread(target=compile, args= [kid, j])
                cthread.start()
                compile_threads.append(cthread)
                thread = threading.Thread(target = run, args=[kid, j, res])
                thread.params = kid
                threads.append(thread)

            # wait for compilation to finish
            for cthread in compile_threads:
                cthread.join()
                
            # start the factoring
            for thread, idx in zip(threads, count()):
                if VERBOSE:
                    print("started fct{}".format(idx))
                thread.start()
                
            # give them time to factor
            sleep(4)
            for thread in threads:
                thread.join(0)
            

            # KILL off the slow ones
            for j in range(N_THREADS):
                if threads[j].is_alive():
                    if VERBOSE:
                        print("fct{} is still running".format(j))
                    call("killall fct{}".format(j), shell=True)
                else:
                    try:
                        f = open('res{}'.format(i), 'r')
                        time = float(f.readlines()[0].split()[1])
                        res.append((time, threads[j].params))
                    except Exception as e: 
                        print(e)
            
    
    best= sorted(res)[:N]
    bestt, bestp = best[0]
    if bestt<BEST_TIME:
        BEST_TIME = bestt
        BEST_PARAMS = bestp

    write(bestp, 0)
    call("cp constants0.h constants.h", shell=True)

    f = open("quadsieve.cpp")
    s = f.read()
    f.close()
    s.replace("get_si()", "get_mpz_t()->_mp_d[0]")
    f = open("quadsieve.cpp", "w")
    f.write(s);
    f.close()

    call("make -s submit ", shell=True)

    f = open("quadsieve.cpp")
    s = f.read()
    f.close()
    s.replace("get_mpz_t()->_mp_d[0]", "get_si()")
    f = open("quadsieve.cpp", "w")
    f.write(s);
    f.close()

    write(best[1][1], 0)
    call("cp constants0.h constants.h", shell=True)
    call("make -s submit", shell=True)
    
    res_file = open("iteration{}".format(it), 'w')

    for time, params in best:
        print("{}\t {}".format(time, params), file=res_file)
    res_file.close()
    return [x[1] for x in best]

def genetic(N, k):
    current_generation = [START_PARAMS for i in range(N)] 

    for i in range(k):
        current_generation = next_generation(current_generation, 8, i, N)

if __name__ == "__main__":
    genetic(8, 200)

